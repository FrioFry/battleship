#include <Game.h>
#include <ComputerStrategy.h>
#include <KeyboardStrategy.h>

void main()
{
	ComputerStrategy s1;
	ComputerStrategy s2;
	//KeyboardStrategy s2;

	Game g(s1, s2);
	g.Run();
}