#include "GameField.h"

namespace
{

GameShip *ShipByHit(GameShips &ships, int row, int col)
{
	Cell hit(row, col);
	for (GameShip &ship : ships)
	{
		// Ship is already destroyed
		if (ship.hits_left <= 0)
		{
			continue;
		}

		for (GameCell &ship_cell : ship.cells)
		{
			if (ship_cell == hit)
			{
				return &ship;
			}
		}
	}
	return nullptr;
}

void MarkZonesNearShips(ViewForOwner &owner_view)
{
	// Init ships zones for owner
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 10; j++)
		{
			if (owner_view[i][j] == OwnerCell::Ship)
			{
				// Place 8 zone cells near ship
				for (int k = i - 1; k <= i + 1; k++)
				{
					for (int t = j - 1; t <= j + 1; t++)
					{
						if (k < 0 || t < 0 || k >= 10 || t >= 10)
						{
							continue;
						}
						if (owner_view[k][t] == OwnerCell::Empty)
						{
							owner_view[k][t] = OwnerCell::ShipZone;
						}
					}
				}
			}
		}
	}
}

void MarkZonesAfterEnemyHit(ViewForEnemy &enemy_view, int i, int j, bool destroyed)
{
	bool has_vertical_neighbour = false;
	bool has_horizontal_neighbour = false;

	if (!destroyed)
	{
		// Up - Down
		if ((i > 0 && enemy_view[i - 1][j] == EnemyCell::HitShip) ||
			(i < 9 && enemy_view[i + 1][j] == EnemyCell::HitShip) )
		{
			has_vertical_neighbour = true;
		}

		// Left-Right
		if ((j > 0 && enemy_view[i][j - 1] == EnemyCell::HitShip) ||
			(j < 9 && enemy_view[i][j + 1] == EnemyCell::HitShip))
		{
			has_horizontal_neighbour = true;
		}
		if (!has_horizontal_neighbour && !has_vertical_neighbour)
		{
			return;
		}
	}


	// Mark Zones
	// Place 8 zone cells near ship
	for (int k = i - 1; k <= i + 1; k++)
	{
		for (int t = j - 1; t <= j + 1; t++)
		{
			if (k < 0 || t < 0 || k >= 10 || t >= 10)
			{
				continue;
			}

			if (!destroyed)
			{
				if (k == i && has_horizontal_neighbour || 
					t == j && has_vertical_neighbour)
				{
					continue;
				}
			}
			
			if (enemy_view[k][t] == EnemyCell::Unknown)
			{
				enemy_view[k][t] = EnemyCell::ShipZone;
			}
		}
	}
}

} // namespace

GameField::GameField(const SetupShips &setup_ships)
	: still_alive_count_(4, 0)
{
	// Init owner, enemy views 
	owner_view_.resize(10);
	enemy_view_.resize(10);
	for (int i = 0; i < 10; ++i)
	{
		OwnerRow &owner_row = owner_view_[i];
		EnemyRow &enemy_row = enemy_view_[i];

		owner_row.resize(10);
		enemy_row.resize(10);
		for (int j = 0; j < 10; j++)
		{
			owner_view_[i][j] = OwnerCell::Empty;
			enemy_view_[i][j] = EnemyCell::Unknown;
		}
	}

	// Init ships
	for (auto ship : setup_ships)
	{
		GameShip g_ship;
		g_ship.hits_left = ship.size();
		still_alive_count_[ship.size() - 1]++;
		for (auto cell : ship)
		{
			GameCell g_cell(cell.row, cell.col);
			g_ship.cells.push_back(g_cell);
			owner_view_[cell.row][cell.col] = OwnerCell::Ship;
		}
		ships_.push_back(g_ship);
	}

	MarkZonesNearShips(owner_view_);
}

bool GameField::Defeated()
{
	static std::vector<unsigned int> s_zero_hits({ 0, 0, 0, 0 });
	return still_alive_count_ == s_zero_hits;
}


HitStatus GameField::Hit(int row, int col)
{
	OwnerCell &owner_cell = owner_view_[row][col];
	EnemyCell &enemy_cell = enemy_view_[row][col];

	// Check if miss
	if (owner_cell != OwnerCell::Ship)
	{
		if (owner_cell != OwnerCell::HitShip)
		{
			owner_cell = OwnerCell::HitMiss;
			enemy_cell = EnemyCell::HitMiss;
		}
		return HitStatus::Miss;
	}
	owner_cell = OwnerCell::HitShip;
	enemy_cell = EnemyCell::HitShip;

	// Find Ship by coordinates
	GameShip *ship = ShipByHit(ships_, row, col);
	if (ship && ship->hits_left)
	{
		Cell hit(row, col);
		for (const Cell &cell : ship->cells)
		{
			if (cell == hit)
			{
				ship->hits_left--;
				if (ship->hits_left == 0)
				{
					still_alive_count_[ship->cells.size() - 1] -= 1;
				}
				MarkZonesAfterEnemyHit(enemy_view_, row, col, ship->hits_left == 0);
			}
		}
		if (Defeated())
		{
			return HitStatus::MoveIsOver;
		}
		return ship->hits_left ? HitStatus::Hit : HitStatus::Destroyed;
	}

	return HitStatus::Miss;
}
