#include "GameSetup.h"


GameSetup::GameSetup()
	: required_count_({4, 3, 2, 1})
{
}

//! Adds new ship to field
GameSetup::SetShipResult GameSetup::AddShip(const SetupShip& new_ship)
{
	int len = new_ship.size();
	if (len > 4)
	{
		return SetShipResult::InvalidLength;
	}
	if (required_count_[len - 1] == 0)
	{
		return SetShipResult::ImpossibleToPlaceMoreOfThisKind;
	}

	// Check collissions
	for (const SetupShip &ship : ships_)
	{
		for (const Cell &cell_field : ship)
		{
			for (const Cell &cell_new : new_ship)
			{
				if (cell_field == cell_new)
				{
					return SetShipResult::IntersectsWithShip;
				}
				if (intersect_zones(cell_field, cell_new))
				{
					return SetShipResult::ItersectsWithShipsZone;
				}
			}
		}
	}

	// Ship is fine
	ships_.push_back(new_ship);
	required_count_[len - 1]--;
	return SetShipResult::Success;
}

//! Checks if ship setup is complete
bool GameSetup::Complete()
{
	static std::vector<unsigned int> s_zero_hits = {0,0,0,0};
	return required_count_ == s_zero_hits;
}

//! Returns current ships location
const SetupShips &GameSetup::CurrentShips() const
{
	return ships_;
}