#pragma once
#include <vector>
#include <math.h>

//! Battle field single cell
struct Cell
{
	Cell(int row, int col)
		: row(row)
		, col(col)
	{}
	int row;
	int col;
};

typedef std::vector<Cell> SetupShip;
typedef std::vector<SetupShip> SetupShips;

bool operator==(const Cell &c1, const Cell &c2);

bool intersect_zones(const Cell &c1, const Cell &c2);

enum class ShipOrientation
{
	Vertical,
	Horizontal
};

SetupShip create_ship(int row, int col, ShipOrientation orient, int len);