#pragma once
#include "IStrategy.h"
#include "GameField.h"
#include <memory>

struct Player
{
	Player(IStrategy &s, int id)
		: strategy(s)
		, id(id)
		, enemy_field(nullptr)
		, field(nullptr)
	{}
	IStrategy &strategy;

	GameField *enemy_field;
	GameField *field;
	int id;
};

enum class GamePhase
{
	Uninitialized,
	Setup,
	SetupEnd,
	Battle,
	Results,
	End
};

//! Battlefield game manager
class Game
{
public:
	Game(IStrategy &player1, IStrategy &player2);
	GamePhase CurrentPhase();
	void Run();	

protected:
	void Setup();
	void Play();
	void End();

private:
	Player player1_;
	Player player2_;
	std::unique_ptr<GameField> field1_;
	std::unique_ptr<GameField> field2_;
	GamePhase phase_;
};
