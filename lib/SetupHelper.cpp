#include "SetupHelper.h"
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <random>

void Rotate90(SetupShips &ships)
{
	for (SetupShip &s : ships)
	{
		for (Cell &c : s)
		{
			int row = c.col;
			int col = 9 - c.row;
			c.row = row;
			c.col = col;
		}
	}
}

void Setup1(SetupShips &ships)
{
	// 4
	ships.push_back(create_ship(0, 0, ShipOrientation::Vertical, 4));

	// 3
	ships.push_back(create_ship(0, 2, ShipOrientation::Vertical, 3));
	ships.push_back(create_ship(4, 2, ShipOrientation::Vertical, 3));

	// 2
	ships.push_back(create_ship(5, 0, ShipOrientation::Vertical, 2));
	ships.push_back(create_ship(8, 0, ShipOrientation::Vertical, 2));
	ships.push_back(create_ship(8, 2, ShipOrientation::Vertical, 2));

	// 1 col>=4
	int i = 0;
	while (i < 4)
	{
		int row = rand() % 10;
		int col = 4 + rand() % 6;
		SetupShip new_ship = create_ship(row, col, ShipOrientation::Vertical, 1);
		bool intersects = false;
		for (const SetupShip &s : ships)
		{
			for (const Cell &c : s)
			{
				if (intersect_zones(new_ship[0], c))
				{
					intersects = true;
					break;
				}
			}
			if (intersects)
			{
				break;
			}
		}
		if (!intersects)
		{
			i++;
			ships.push_back(new_ship);
		}
	}
}

void Setup2(SetupShips &ships)
{
	// 4
	ships.push_back(create_ship(0, 0, ShipOrientation::Vertical, 4));

	// 3
	ships.push_back(create_ship(0, 2, ShipOrientation::Horizontal, 3));
	ships.push_back(create_ship(4, 0, ShipOrientation::Vertical, 3));

	// 2
	ships.push_back(create_ship(9, 0, ShipOrientation::Horizontal, 2));
	ships.push_back(create_ship(0, 6, ShipOrientation::Horizontal, 2));
	ships.push_back(create_ship(0, 9, ShipOrientation::Vertical, 2));

	// 1 col>=2, row >= 2
	int i = 0;
	while (i < 4)
	{
		int row = 2 + rand() % 8;
		int col = 2 + rand() % 8;
		SetupShip new_ship = create_ship(row, col, ShipOrientation::Vertical, 1);
		bool intersects = false;
		for (const SetupShip &s : ships)
		{
			for (const Cell &c : s)
			{
				if (intersect_zones(new_ship[0], c))
				{
					intersects = true;
					break;
				}
			}
			if (intersects)
			{
				break;
			}
		}
		if (!intersects)
		{
			i++;
			ships.push_back(new_ship);
		}
	}
}

void OptRandomSetup(SetupShips &ships, int seed)
{
	srand(unsigned(time(NULL)+seed));

	// Choose initial position
	if (rand() % 2)
	{
		Setup1(ships);
	}
	else
	{
		Setup2(ships);
	}
	// TODO: add more initial placements

	// Rotate ships
	int rotates = rand() % 4;
	for (int i = 0; i < rotates; ++i)
	{
		Rotate90(ships);
	}
}
