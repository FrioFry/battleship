import os

content = [x for x in os.listdir('.') if x.endswith('.cpp')]

prefix = """call "D:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\bin\\amd64\\vcvars64.bat"

set PATH=C:\\Work\\cclash\\CClash\\bin\\Release\\;%PATH%
set CCLASH_SERVER=
set CCLASH_HARDLINK=
set CCLASH_Z7_OBJ=
set CCLASH_DIR=C:\work\cclash_dir

"""

template = 'cl.exe /EHsc /O2 /c %s -I. /D"UNICODE" /D"_UNICODE"\n'

compile_str = ""
for f in content:
	compile_str += template % f

link_str = 'link.exe %s /OUT:result.exe' % (" ".join([x.replace('.cpp', '.obj') for x in content]))

with open('do.bat', 'w') as f:
	f.write(prefix)
	f.write(compile_str)
	f.write(link_str)
