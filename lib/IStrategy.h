#pragma once

#include "GameMove.h"
#include "GameSetup.h"
#include "GameField.h"
#include "World.h"

//! This iterface represents players battleship strategy. Need to be implemented by user.
class IStrategy
{
public:

	//! Initialize players ships locatoin before game starts
	virtual void OnSetup(GameSetup &ship_setup) = 0;
	
	//! In-game Move
	virtual void OnMove(GameMove &move, const World &world) = 0;

	//! Game result
	virtual void OnGameEnd(bool win, int player_id) = 0;

	virtual ~IStrategy() {};
};
