#include "Ship.h"

bool operator==(const Cell &c1, const Cell &c2)
{
	return c1.row == c2.row && c1.col == c2.col;
}

bool intersect_zones(const Cell &c1, const Cell &c2)
{
	return abs(c1.row - c2.row) <= 1 && abs(c1.col - c2.col) <= 1;
}

SetupShip create_ship(int row, int col, ShipOrientation orient, int len)
{
	SetupShip res;
	int r = row;
	int c = col;
	for (int i = 0; i < len; i++)
	{
		Cell cell(r, c);
		if (orient == ShipOrientation::Vertical)
		{
			r++;
		}
		else
		{
			c++;
		}
		res.push_back(cell);
	}
	return res;
}
