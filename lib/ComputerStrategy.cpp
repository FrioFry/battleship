#include "ComputerStrategy.h"
#include "SetupHelper.h"
#include "ConsoleIO.h"

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <random>
#include <iterator>
#include <windows.h>
namespace
{
//! Fills single diagonal in Field
void DiagonalFill(std::vector<Cell> &moves, const Cell &start, int d_row, int d_col)
{
	int row = start.row;
	int col = start.col;
	while (row <= 9 && col <= 9 && row >= 0 && col >= 0)
	{
		moves.push_back(Cell(row, col));
		row += d_row;
		col += d_col;
	}
}

//! Fills list of diagonals with defined starts
void DiagonalsFill(std::vector<Cell> &moves, const std::vector<Cell> &starts, int d_row, int d_col)
{
	for (const Cell &start : starts)
	{
		DiagonalFill(moves, start, d_row, d_col);
	}
}

//! Add optimal moves to catch 4-len ship
void AddMoves4(ComputerStrategy::MoveList &move_list)
{
	std::vector<Cell> moves;
	srand(unsigned(time(NULL)));
	bool fwd = rand() % 2 == 1;
	if (fwd)
	{
		std::vector<Cell> starts = {
			Cell(3, 0),
			Cell(7, 0),
			Cell(9, 2),
			Cell(9, 6) };

		// down-left to upper-right
		DiagonalsFill(moves, starts, -1, +1);
	}
	else
	{
		std::vector<Cell> starts = {
			Cell(3, 9),
			Cell(7, 9),
			Cell(9, 3),
			Cell(9, 7)};

		// down-right to upper-left
		DiagonalsFill(moves, starts, -1, -1);
	}

	std::shuffle(moves.begin(), moves.end(), 
		std::default_random_engine(unsigned(time(NULL))));
	std::copy(moves.begin(), moves.end(), std::back_inserter(move_list));

}

//! Add optimal moves to catch 3-len ship
void AddMoves23(ComputerStrategy::MoveList &move_list)
{
	std::vector<Cell> moves;
	srand(unsigned(time(NULL)));
	bool fwd = rand() % 2 == 1;
	if (fwd)
	{
		std::vector<Cell> starts = {
			Cell(1, 0),
			Cell(5, 0),
			Cell(9, 0),
			Cell(9, 4),
			Cell(9, 8) };

		// down-left to upper-right
		DiagonalsFill(moves, starts, -1, +1);
	}
	else
	{
		std::vector<Cell> starts = {
			Cell(1, 9),
			Cell(5, 9),
			Cell(9, 9),
			Cell(9, 5),
			Cell(9, 1) };

		// down-right to upper-left
		DiagonalsFill(moves, starts, -1, -1);
	}

	std::shuffle(moves.begin(), moves.end(),
		std::default_random_engine(unsigned(time(NULL))));
	std::copy(moves.begin(), moves.end(), std::back_inserter(move_list));
}

//! Add  moves to catch 1-len ships
void AddMoves1(ComputerStrategy::MoveList &move_list, const ViewForEnemy &field)
{
	std::vector<Cell> moves;

	// Find all Unknown cells:
	for (unsigned row = 0; row < field.size(); row++)
	{
		const EnemyRow &r = field[row];
		for (unsigned col = 0; col < r.size(); col++)
		{
			if (r[col] == EnemyCell::Unknown)
			{
				moves.push_back(Cell(row, col));
			}
		}
	}
	std::shuffle(moves.begin(), moves.end(),
		std::default_random_engine(unsigned(time(NULL))));
	std::copy(moves.begin(), moves.end(), std::back_inserter(move_list));
}

//! Adds to front of move list new hits 
void AddMovesAfterHit(ComputerStrategy::MoveList &move_list, const ViewForEnemy &field, const Cell &hit)
{
	std::vector<Cell> new_moves;
	bool has_vertical_neighbour = false;
	bool has_horizontal_neighbour = false;
	int i = hit.row;
	int j = hit.col;
	// look vertical
	if ((i > 0 && field[i - 1][j] == EnemyCell::HitShip) || 
		(i < 9 && field[i + 1][j] == EnemyCell::HitShip))
	{
		has_vertical_neighbour = true;
	}

	// look horizontal
	if ((j > 0 && field[i][j - 1] == EnemyCell::HitShip) ||
		(j < 9 && field[i][j + 1] == EnemyCell::HitShip))
	{
		has_horizontal_neighbour = true;
	}

	// move Up, down
	if (!has_horizontal_neighbour)
	{
		if (i > 0 && field[i - 1][j] == EnemyCell::Unknown)
		{
			new_moves.push_back(Cell(i - 1, j));
		}
		if (i < 9 && field[i + 1][j] == EnemyCell::Unknown)
		{
			new_moves.push_back(Cell(i + 1, j));
		}
	}

	// move left, right
	if (!has_vertical_neighbour)
	{
		if (j > 0 && field[i][j - 1] == EnemyCell::Unknown)
		{
			new_moves.push_back(Cell(i, j - 1));
		}
		if (j < 9 && field[i][j + 1] == EnemyCell::Unknown)
		{
			new_moves.push_back(Cell(i, j + 1));
		}
	}
	std::shuffle(new_moves.begin(), new_moves.end(), std::default_random_engine(unsigned(time(NULL))));
	std::copy(new_moves.begin(), new_moves.end(), std::front_inserter(move_list));
}

} // namespace 

ComputerStrategy::ComputerStrategy()
	:phase_(Phase::Four)
{
	AddMoves4(move_list_);
}

ComputerStrategy::~ComputerStrategy()
{}

void ComputerStrategy::OnSetup(GameSetup &ship_setup)
{
	SetupShips ships;
	OptRandomSetup(ships, (int)this);
	// Fill 
	for (const SetupShip s : ships)
	{
		ship_setup.AddShip(s);
	}
}

void ComputerStrategy::OnMove(GameMove &move, const World &world)
{
	const ViewForEnemy &enemy_field = *world.enemy_field;
	const auto &enemy_alive_counts = *world.enemy_alive_count;

	while (!move.IsOver() && !move_list_.empty())
	{
		if (world.player_id == 1)
		{
			OutputGUI(world);
			Sleep(800);
		}
		const Cell &c = move_list_.front();
		move_list_.pop_front();

		if (enemy_field[c.row][c.col] != EnemyCell::Unknown)
		{
			continue;
		}
		HitStatus res = move.Hit(c.row, c.col);

		//! If we find some ship, need to correct move_list
		if (res == HitStatus::Hit)
		{
			AddMovesAfterHit(move_list_, enemy_field, c);
		}
		if (res == HitStatus::Destroyed)
		{
			// Four-lenghed ship is destroyed, lets find 2 and 3-length ships
			if (enemy_alive_counts[3] == 0 && phase_ == Phase::Four)
			{
				phase_ = Phase::TwoAndThrees;
				AddMoves23(move_list_);
			}
			// Only signle-lenght ships are left;
			if (enemy_alive_counts[2] == 0 && enemy_alive_counts[1] == 0 &&
				phase_ == Phase::TwoAndThrees)
			{
				phase_ = Phase::Singles;
				AddMoves1(move_list_, enemy_field);
			}
		}
	}
	
}

void ComputerStrategy::OnGameEnd(bool win, int player_id)
{
	if (player_id == 1)
	{
		if (win)
		{
			std::cout << "Victory" << std::endl;
		}
		else
		{
			std::cout << "Lose" << std::endl;
		}
		std::cout << "Press any key..." << std::endl;
		char c;
		std::cin >> c;
	}

}