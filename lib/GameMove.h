#pragma once
#include "GameField.h"

//! Handles moves during battle
class GameMove
{

public:
	GameMove(GameField *enemy_field);
	//! Hit at enemy field
	HitStatus Hit(int row, int col);

	//! Checks if current move is over
	bool IsOver();

private:
	GameField *enemy_field_;
	bool is_over_;
};