#include "GameMove.h"


GameMove::GameMove(GameField *enemy_field)
	: enemy_field_(enemy_field)
	, is_over_(false)
{
}


HitStatus GameMove::Hit(int row, int col)
{
	if (IsOver())
	{
		return HitStatus::MoveIsOver;
	}
	HitStatus status = enemy_field_->Hit(row, col);
	if (status != HitStatus::Hit &&
		status != HitStatus::Destroyed)
	{
		is_over_ = true;
	}
	
	return status;
}

bool GameMove::IsOver()
{
	return is_over_;
}