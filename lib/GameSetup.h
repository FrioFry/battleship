#pragma once
#include "Ship.h"

//! Class to handle ship setup
class GameSetup
{
public:
	//! Ship placement stauts
	enum class SetShipResult
	{
		Success = 0,
		InvalidLength,
		ImpossibleToPlaceMoreOfThisKind,
		IntersectsWithShip,
		ItersectsWithShipsZone
	};

public:
	GameSetup();

	//! Adds new ship to field
	SetShipResult AddShip(const SetupShip& ship);

	//! Checks if ship setup is complete
	bool Complete();

	//! Returns current ships location
	const SetupShips &CurrentShips() const;
private:
	SetupShips ships_;
	std::vector<unsigned int> required_count_; //! required count of Ships each type
};

