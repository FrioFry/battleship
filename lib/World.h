#pragma once
#include "GameField.h"

struct World
{
	const std::vector<unsigned int> *enemy_alive_count;
	const ViewForOwner *my_field;
	const ViewForEnemy *enemy_field;
	int player_id;
};
