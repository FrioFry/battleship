#pragma once
#include "IStrategy.h"

class KeyboardStrategy: public IStrategy
{
public:
	KeyboardStrategy();
	virtual void OnSetup(GameSetup &ship_setup);
	virtual void OnMove(GameMove &move, const World &world);
	virtual void OnGameEnd(bool win, int player_id);
private:

};