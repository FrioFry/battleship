#include "ConsoleIO.h"
#include <windows.h>
#include <map>

using namespace std;

void ClearScreen()
{
	char fill = ' ';
	COORD tl = { 0, 0 };
	CONSOLE_SCREEN_BUFFER_INFO s;
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(console, &s);
	DWORD written, cells = s.dwSize.X * s.dwSize.Y;
	FillConsoleOutputCharacter(console, fill, cells, tl, &written);
	FillConsoleOutputAttribute(console, s.wAttributes, cells, tl, &written);
	SetConsoleCursorPosition(console, tl);
}

SetupShip InputShip(int length)
{
	int row = 0;
	int col = 0;
	int horizontal = 0;
	printf("> Set start for %d-length ship (row, col, horizontal), e.g. \"0 0 1\": ", length);
	scanf("%d %d %d", &row, &col, &horizontal);
	SetupShip ship = create_ship(row, col, horizontal ? ShipOrientation::Horizontal : ShipOrientation::Vertical, length);
	printf("\n");
	return ship;
}

SetupShips InputShips()
{
	SetupShips ships;
	
	ships.push_back(InputShip(4));
	ships.push_back(InputShip(3));
	ships.push_back(InputShip(3));
	ships.push_back(InputShip(2));
	ships.push_back(InputShip(2));
	ships.push_back(InputShip(2));
	ships.push_back(InputShip(1));
	ships.push_back(InputShip(1));
	ships.push_back(InputShip(1));
	ships.push_back(InputShip(1));
	return ships;
}

void OutputGUI(const World &world)
{
	static std::map<OwnerCell, char> owner_map = { 
		{ OwnerCell::Empty, '.' },
		{ OwnerCell::HitShip, 'X' },
		{ OwnerCell::HitMiss, 'o' },
		{ OwnerCell::Ship, 'S' },
		{ OwnerCell::ShipZone, '-' }};

	static std::map<EnemyCell, char> enemy_map = {
		{ EnemyCell::Unknown, '.' },
		{ EnemyCell::HitShip, 'X' },
		{ EnemyCell::HitMiss, 'o' },
		{ EnemyCell::ShipZone, '-' } };

	const ViewForOwner &my_field = *world.my_field;
	const ViewForEnemy &enemy_field = *world.enemy_field;
	const std::vector<unsigned> &ships_count = *world.enemy_alive_count;
	ClearScreen();
	// header
	printf("               ME       #           ENEMY\n");
	printf("                        #                        #\n");
	printf(" #  0 1 2 3 4 5 6 7 8 9 # #  0 1 2 3 4 5 6 7 8 9 #\n");
	for (int i = 0; i < 10; i++)
	{
		printf(" %d ", i);
		for (int j = 0; j < 10; j++)
		{
			OwnerCell cell = my_field[i][j];
			char c = owner_map[cell];
			printf(" %c", c);
		}
		printf(" # %d ", i);
		for (int j = 0; j < 10; j++)
		{
			EnemyCell cell = enemy_field[i][j];
			char c = enemy_map[cell];
			printf(" %c", c);
		}
		printf(" #\n");
	}
	printf(" #################################################\n");

	printf("Enemy ships (single: %d, double: %d, triple: %d , quatro: %d)\n", ships_count[0], ships_count[1], ships_count[2], ships_count[3]);
}

Cell InputFirePosition()
{
	int row = -1;
	int col = -1;
	while (row < 0 || row > 9 || col < 0 || col > 9)
	{
		printf("> Fire to ship (row, col), e.g. \"0 9\": \n");
		scanf("%d %d", &row, &col);
		printf("\n");
	}
	return Cell(row, col);
}


void DisplayHitResult(const HitStatus &hit_result, const Cell &cell)
{
	printf("> Fire at (%d, %d) ", cell.row, cell.col);
	if (hit_result == HitStatus::Destroyed)
	{ 
		printf("Destroyed\n");
	}
	if (hit_result == HitStatus::Hit)
	{
		printf("Hit\n");
	}
	if (hit_result == HitStatus::Miss)
	{
		printf("Miss\n");
	}
	if (hit_result == HitStatus::MoveIsOver)
	{
		printf("Turn is over\n");
	}
	Sleep(500);
}

void DisplayGameResult(bool win)
{
	if (win)
	{
		printf("> CONGRATULATIONS \n");
	}
	else
	{
		printf("> LOST \n");
	}
}
