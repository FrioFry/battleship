#include "KeyboardStrategy.h"
#include "ConsoleIO.h"
#include "SetupHelper.h"

KeyboardStrategy::KeyboardStrategy()
{
}

void KeyboardStrategy::OnSetup(GameSetup &ship_setup)
{
	//SetupShips ships = InputShips();
	SetupShips ships;
	OptRandomSetup(ships);
	for (const SetupShip &ship : ships)
	{
		ship_setup.AddShip(ship);
	}
}

void KeyboardStrategy::OnMove(GameMove &move, const World &world)
{
	while (!move.IsOver())
	{
		OutputGUI(world);
		Cell fire = InputFirePosition();
		HitStatus res = move.Hit(fire.row, fire.col);
		DisplayHitResult(res, fire);
	}
}

void KeyboardStrategy::OnGameEnd(bool win, int player_id)
{
	DisplayGameResult(win);
}