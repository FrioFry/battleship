#include "Game.h"
#include "GameSetup.h"
#include <thread>
#include <stdlib.h>
#include <time.h>
#include <algorithm>


namespace
{

void SetupFunc(IStrategy *strategy, GameSetup *ship_setupper)
{
	while (!ship_setupper->Complete())
	{
		strategy->OnSetup(*ship_setupper);
	}
}

void ResultFunc(IStrategy *strategy, bool win, int player_id)
{
	strategy->OnGameEnd(win, player_id);
}

} // namespace

Game::Game(IStrategy &strategy1, IStrategy &strategy2)
	: player1_(strategy1, 1)
	, player2_(strategy2, 2)
	, phase_(GamePhase::Uninitialized)
{
}

void Game::Setup()
{
	if (phase_ == GamePhase::Uninitialized)
	{
		phase_ = GamePhase::Setup;
		GameSetup setup1;
		GameSetup setup2;
		std::thread t1(SetupFunc, &player1_.strategy, &setup1);
		std::thread t2(SetupFunc, &player2_.strategy, &setup2);
		t1.join();
		t2.join();

		auto ships1 = setup1.CurrentShips();
		auto ships2 = setup2.CurrentShips();

		field1_ = std::make_unique<GameField>(ships1);
		field2_ = std::make_unique<GameField>(ships2);

		player1_.field = field1_.get();
		player1_.enemy_field = field2_.get();

		player2_.field = player1_.enemy_field;
		player2_.enemy_field = player1_.field;

		phase_ = GamePhase::SetupEnd;

		Play();
	}
}

void Game::Play()
{
	if (phase_ == GamePhase::SetupEnd)
	{
		phase_ = GamePhase::Battle;

		// Random first player
		srand(unsigned(time(NULL)));
		Player *current = &player1_;
		Player *next = &player2_;
		int whos_first = rand() % 2;
		if (whos_first)
		{
			std::swap(current, next);
		}

		// main loop
		while (1)
		{
			GameMove move(current->enemy_field);

			while (!move.IsOver())
			{
				World world;
				world.enemy_field = &current->enemy_field->enemy_view_;
				world.my_field = &current->field->owner_view_;
				world.enemy_alive_count = &current->enemy_field->still_alive_count_;
				world.player_id = current->id;

				current->strategy.OnMove(move, world);
				if (current->enemy_field->Defeated())
				{
					phase_ = GamePhase::Results;
					End();
					return;
				}
			}
			std::swap(current, next);
		}
	}
}

void Game::End()
{
	if (phase_ == GamePhase::Results)
	{
		bool player1_win = player1_.enemy_field->Defeated();
		std::thread t1(ResultFunc, &player1_.strategy, player1_win, 1);
		std::thread t2(ResultFunc, &player2_.strategy, !player1_win, 2);
		t1.join();
		t2.join();
		phase_ = GamePhase::End;
	}
}

void Game::Run()
{
	Setup();
}

GamePhase Game::CurrentPhase()
{
	return phase_;
}