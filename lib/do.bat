call "D:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\amd64\vcvars64.bat"

set PATH=C:\Work\cclash\CClash\bin\Release\;%PATH%
set CCLASH_SERVER=
set CCLASH_HARDLINK=
set CCLASH_Z7_OBJ=
set CCLASH_DIR=C:\work\cclash_dir

cl.exe /EHsc /O2 /c ComputerStrategy.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c ConsoleIO.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c Game.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c GameField.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c GameMove.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c GameSetup.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c KeyboardStrategy.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c main.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c SetupHelper.cpp -I. /D"UNICODE" /D"_UNICODE"
cl.exe /EHsc /O2 /c Ship.cpp -I. /D"UNICODE" /D"_UNICODE"
link.exe ComputerStrategy.obj ConsoleIO.obj Game.obj GameField.obj GameMove.obj GameSetup.obj KeyboardStrategy.obj main.obj SetupHelper.obj Ship.obj /OUT:result.exe