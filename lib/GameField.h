#pragma once

#include <vector>
#include "Ship.h"

enum class HitStatus
{
	Hit, //! Ship is hit but not destroyed
	Destroyed, //! ship is completely destroyed
	Miss,
	MoveIsOver //! no more actions allowed in that move
};

enum class OwnerCell
{
	Empty, 
	HitShip, //! Wounded ship
	HitMiss,
	Ship, //! Not damaged ship (only for current player)
	ShipZone //! Area near ship 
};
typedef std::vector<OwnerCell> OwnerRow;
typedef std::vector<OwnerRow> ViewForOwner;

enum class EnemyCell
{
	Unknown, //! Unknown territory 
	HitShip, //! Wounded ship
	HitMiss, 
	ShipZone //! Area near ship 
};

typedef std::vector<EnemyCell> EnemyRow;
typedef std::vector<EnemyRow> ViewForEnemy;


struct GameCell: public Cell
{
public:
	GameCell(int row, int col)
		: Cell(row, col)
		, state(OwnerCell::Empty)
	{}

	OwnerCell state;
};

struct GameShip
{
	int hits_left;
	std::vector<GameCell> cells;
};

typedef std::vector<GameShip> GameShips;

/*! Class represents internal data for game 
* ships - is list of ships and their hit points
* table - complete game table
*/
class GameField
{
public:
	GameField(const SetupShips &setup_ships);
	bool Defeated();
	HitStatus Hit(int row, int col);

public:
	std::vector<unsigned int> still_alive_count_;
	GameShips ships_;
	ViewForOwner owner_view_;
	ViewForEnemy enemy_view_;
};


