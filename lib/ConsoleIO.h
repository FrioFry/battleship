#pragma once
#include "Ship.h"
#include "World.h"

SetupShip InputShip(int length);
SetupShips InputShips();

void OutputGUI(const World &world);
Cell InputFirePosition();
void ClearScreen();

void DisplayHitResult(const HitStatus &hit_result, const Cell &cell);
void DisplayGameResult(bool win);