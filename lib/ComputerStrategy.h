#pragma once
#include <deque>

#include "IStrategy.h"

enum class Phase
{
	Four,
	TwoAndThrees,
	Singles,
};
class ComputerStrategy : public IStrategy
{
public:
	ComputerStrategy();
	virtual void OnSetup(GameSetup &ship_setup);
	virtual void OnMove(GameMove &move, const World &world);
	virtual void OnGameEnd(bool win, int player_id);
	virtual ~ComputerStrategy();

public:
	typedef std::deque<Cell> MoveList;

private:
	Phase phase_;
	MoveList move_list_;
};